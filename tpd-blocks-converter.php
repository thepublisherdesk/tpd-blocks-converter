<?php
/**
 *
 * @link https://organicthemes.com
 * @since 1.0.0
 * @package Organic_Widgets
 *
 * @wordpress-plugin
 * Plugin Name: The Publisher Desk - Block Converter
 * Plugin URI: https://publisherdesk.com/
 * Description: Convert all classic content to blocks. An extremely useful tool when upgrading to the WordPress 5 Gutenberg editor.
 * Version: 1.0.0
 * Author: The Publisher Desk
 * Author URI: https://publisherdesk.com/
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: tpd
 * Domain Path: /languages/
 */

define('TPD_BLOCK_CONVERTER_PATH', plugin_dir_path( __FILE__ ) );
define('TPD_BLOCK_CONVERTER_URI', plugin_dir_url( __FILE__ ) );
// post types and statuses plugin work with
define( 'TPD_BLOCK_CONVERTER_TYPES', serialize( array( 'post' ) ) );
define( 'TPD_BLOCK_CONVERTER_STATUSES', serialize( array( 'publish', 'future', 'draft', 'private' ) ) );

if ( ! function_exists( 'tpd_register_block_converter_taxonomy' ) ) {

	// Register Custom Taxonomy
	function tpd_register_block_converter_taxonomy() {
	
		$labels = array(
			'name'                       => _x( 'Classic to Gutenberg Posts', 'Taxonomy General Name', 'tpd' ),
			'singular_name'              => _x( 'Classic to Gutenberg Post', 'Taxonomy Singular Name', 'tpd' ),
			'menu_name'                  => __( 'Classic to Gutenberg Posts', 'tpd' ),
			'all_items'                  => __( 'All Posts', 'tpd' ),
			'parent_item'                => __( 'Parent Post', 'tpd' ),
			'parent_item_colon'          => __( 'Parent Post:', 'tpd' ),
			'new_item_name'              => __( 'New Post Name', 'tpd' ),
			'add_new_item'               => __( 'Add New Post', 'tpd' ),
			'edit_item'                  => __( 'Edit Post', 'tpd' ),
			'update_item'                => __( 'Update Post', 'tpd' ),
			'view_item'                  => __( 'View Post', 'tpd' ),
			'separate_items_with_commas' => __( 'Separate post with commas', 'tpd' ),
			'add_or_remove_items'        => __( 'Add or remove posts', 'tpd' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'tpd' ),
			'popular_items'              => __( 'Popular Items', 'tpd' ),
			'search_items'               => __( 'Search Items', 'tpd' ),
			'not_found'                  => __( 'Not Found', 'tpd' ),
			'no_terms'                   => __( 'No items', 'tpd' ),
			'items_list'                 => __( 'Items list', 'tpd' ),
			'items_list_navigation'      => __( 'Items list navigation', 'tpd' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'show_in_rest'               => true,
		);
		register_taxonomy( 'tpd_blocks_converter', array( 'post' ), $args );

		wp_insert_term(
			'Classic Editor',   // the term 
			'tpd_blocks_converter', // the taxonomy
			array(
				'description' => 'Post using classic editor.',
				'slug'        => 'classic',
			)
		);
		wp_cache_flush();
	}
	add_action( 'init', 'tpd_register_block_converter_taxonomy', 0 );
	
}
/**
 * Register a custom menu page.
 */
function tpd_add_block_converter_admin_page() {
    add_submenu_page( 
        'tools.php',
        'Classic to Gutenberg Conversion',
        'Classic to Gutenberg',
        'manage_options',
        'tpd-block-converter',
        'tpd_block_converter_render_index',
    );
}
add_action( 'admin_menu', 'tpd_add_block_converter_admin_page' );

/**
 * Enqueue admin styles and scripts.
 */
function tpd_block_converter_enqueue_scripts() {

	wp_register_script( 'tpd-block-converter-script', TPD_BLOCK_CONVERTER_URI . 'admin/assets/convert.js', array( 'jquery', 'wp-blocks', 'wp-edit-post' ), false, true );
	$jsObj = array(
		'ajaxUrl'                      => admin_url( 'admin-ajax.php' ),
		'serverErrorMessage'           => '<div class="error"><p>' . __( 'Server error occured!', 'tpd' ) . '</p></div>',
		'scanningMessage'              => '<p>' . sprintf( __( 'Scanning... %s%%', 'tpd' ), 0 ) . '</p>',
		'bulkConvertingMessage'        => '<p>' . sprintf( __( 'Converting... %s%%', 'tpd' ), 0 ) . '</p>',
		'bulkConvertingSuccessMessage' => '<div class="updated"><p>' . __( 'All posts successfully converted!', 'tpd' ) . '</p></div>',
		'confirmConvertAllMessage'     => __( 'You are about to convert all classic posts to blocks. These changes are irreversible. Convert all classic posts to blocks?', 'tpd' ),
		'convertingSingleMessage'      => __( 'Converting...', 'tpd' ),
		'convertedSingleMessage'       => __( 'Converted', 'tpd' ),
        'failedMessage'                => __( 'Failed', 'tpd' ),
        'hookSuffix' => $GLOBALS['hook_suffix'],
        'requestURI' => $_SERVER['REQUEST_URI'],
	);
	wp_localize_script( 'tpd-block-converter-script', 'tpdConvert', $jsObj );
    wp_enqueue_script( 'tpd-block-converter-script' );
    
    wp_enqueue_style( 'tpd-block-converter-style', TPD_BLOCK_CONVERTER_URI . 'admin/assets/style.css' );
}
add_action( 'admin_enqueue_scripts', 'tpd_block_converter_enqueue_scripts' );
/**
 * Render admin page.
 */
function tpd_block_converter_render_index()
{
	require_once TPD_BLOCK_CONVERTER_PATH . 'admin/admin-list.php';
    require_once TPD_BLOCK_CONVERTER_PATH . 'admin/index.php'; 
}

/**
 * Display table with indexed posts.
 */
function tpd_render_table() {
    require_once TPD_BLOCK_CONVERTER_PATH . 'admin/admin-list.php';
	?>
	<div class="meta-box-sortables ui-sortable">
	<?php
	$table = new TPD_CONVERTER_LIST();
	
	?>
		<form method="post" id="tpd-converter-list">
        <?php
            $table->views();
			$table->prepare_items();
			$table->search_box( __( 'Search', 'tpd' ), 'bbconv-search' );
			$table->display();
		?>
		</form>
	</div>
	<?php
}

/**
 * Get translated status label by slug.
 *
 * @param string $status status slug
 *
 * @return string
 */
function tpd_status_label( $status ) {
	$status_labels = array(
		'any'     => __( 'All', 'tpd' ),
		'publish' => __( 'Published', 'tpd' ),
		'future'  => __( 'Future', 'tpd' ),
		'draft'   => __( 'Drafts', 'tpd' ),
		'private' => __( 'Private', 'tpd' ),
	);

	if ( array_key_exists( $status, $status_labels ) ) {
		return $status_labels[ $status ];
	}
	return $status;
}


add_action( 'rest_api_init', function () {
    register_rest_route( 'tpd-blocks-converter/v1', '/single/convert/', array(
        'methods' => 'GET',
        'callback' => 'tpd_convert_single',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'tpd-blocks-converter/v1', '/single/update/', array(
        'methods' => 'POST',
        'callback' => 'tpd_save_single',
    ) );
} );
add_action( 'rest_api_init', function () {
    register_rest_route( 'tpd-blocks-converter/v1', '/bulk/convert/', array(
        'methods' => 'GET',
        'callback' => 'tpd_convert_bulk',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'tpd-blocks-converter/v1', '/bulk/update/', array(
        'methods' => 'POST',
        'callback' => 'tpd_save_bulk',
    ) );
} );
add_action( 'rest_api_init', function () {
    register_rest_route( 'tpd-blocks-converter/v1', '/scan/', array(
        'methods' => 'GET',
        'callback' => 'tpd_scan_posts',
    ) );
} );

/**
 * Find content created in Classic editor
 *
 * @param string $content the content of a post
 *
 * @return bool
 */
function tpd_is_classic_editor( $content )
{
	if ( ! empty( $content ) && strpos( $content, '<!-- wp:' ) === false ) {
		return true;
	}
	return false;
}

function tpd_scan_posts() {

    if(!isset($GLOBALS['hook_suffix'])){
        //$GLOBALS['hook_suffix'] = $_GET['hook_suffix']; //admin_head-{$hook_suffix}
    }

    /** Load WordPress Administration APIs */
    // require_once( ABSPATH . 'wp-admin/includes/admin.php' );


    // require_once TPD_BLOCK_CONVERTER_PATH . 'admin/admin-list.php';
    
	
	$offset         = intval( $_GET['offset'] );
	$total_expected = intval( $_GET['total'] );

	$total_actual  = 0;
    $post_types    = unserialize( TPD_BLOCK_CONVERTER_TYPES );
    $post_statuses = unserialize( TPD_BLOCK_CONVERTER_STATUSES );
    
	foreach ( $post_types as $type ) {
		$post_statuses = wp_count_posts( $type );
		foreach ( $post_statuses as $status ) {
			$total_actual += (int) $status;
        }
    }

	$json = array(
		'error'   => false,
		'offset'  => $total_actual,
		'total'   => $total_actual,
        'message' => '',
	);

	if ( $total_expected != -1 && $total_expected != $total_actual ) {
		$json['error']   = true;
		$json['message'] = '<div class="error"><p>' . __( 'An error occurred while scanning! Someone added or deleted one or more posts during the scanning process. Try again.', 'tpd' ) . '</p></div>';
		return $json;
    }

	$args = array(
		'post_type'      => $post_types,
		'post_status'    => array_keys(get_object_vars($post_statuses)),
		'posts_per_page' => 10,
		'offset'         => $offset,
	);
	$posts_array = get_posts( $args );

	foreach ( $posts_array as $post ) {

		if ( tpd_is_classic_editor( $post->post_content ) ) {
			// update_post_meta( $post->ID, 'tpd_is_classic_editor', 1 );
			wp_set_object_terms( $post->ID, 'classic', 'tpd_blocks_converter');
            // UPDATE `1Wnat4_postmeta` SET meta_value = '' WHERE meta_key = 'tpd_is_classic_editor'
		}
		$offset++;
    }
    
    // $table = new TPD_CONVERTER_LIST();
    // $json['list'] = $table->ajax_response();
    
    $percentage       = (int) ( $offset / $total_actual * 100 );
    $json['offset']   = $offset;
	$json['percentage']   = $percentage;
	$message = ($percentage == 100) ? 'Complete' : 'Scanning';
	$json['message'] .= '<p>' . sprintf( __( '%s... %s%%', 'tpd' ), $message, $percentage ) . '</p>';

	return $json;
}

function tpd_convert_bulk( $data )
{

	$json  = array();

	if ( ! empty( $_GET['total'] ) ) {
		$offset         = intval( $_GET['offset'] );
		$total_expected = intval( $_GET['total'] );

		$post_types    = unserialize( TPD_BLOCK_CONVERTER_TYPES );
		$post_statuses = unserialize( TPD_BLOCK_CONVERTER_STATUSES );

		// $total_actual = (int) tpd_block_converter_count_posts();  //tpd_get_count( $post_types );
		$total_actual = 0;
		$tpd_block_converter_count_posts = tpd_block_converter_count_posts();
		foreach ( $tpd_block_converter_count_posts as $status ) {
			$total_actual += (int) $status;
        }
		if ( $total_expected == -1 ) {
			$total_expected = $total_actual;
		}

		$json = array(
			'error'     => false,
			'offset'    => $total_expected,
			'total'     => $total_expected,
			'message'   => '',
			'postsData' => array(),
		);

		if ( $total_expected != ( $total_actual + $offset ) ) {
			$json['error']   = true;
			$json['message'] = '<div class="error"><p>' . __( 'An error occurred while bulk converting! Someone added or deleted one or more posts during the converting process. Try again.', 'tpd' ) . '</p></div>';
			return $json;
		}

		$args        = array(
			'post_type'      => $post_types,
			'post_status'    => $post_statuses,
			'posts_per_page' => 10,
			'tax_query'		 => array(
				array(
					'taxonomy' => 'tpd_blocks_converter',
					'field'	   => 'slug',
					'terms'    => 'classic'
				)
			),
			// 'meta_key'       => 'tpd_is_classic_editor',
			// 'meta_value'     => 1,
		);
		$posts_array = get_posts( $args );

		$posts_data = array();
		foreach ( $posts_array as $key => $post ) {
			$posts_data[$key] = array(
				'id'      => $post->ID,
				'content' => wpautop( $post->post_content ),
			);
			
			if(wpdocs_detect_shortcode( $post->post_content, 'gallery' ) !== false){
				$posts_data[$key]['gallery'] = wpdocs_detect_shortcode( $post->post_content, 'gallery' );
			}
			$offset++;
		}
		$json['postsData'] = $posts_data;

		$json['offset']  = $offset;
		$percentage      = (int) ( $offset / $total_expected * 100 );
		$json['percentage']  = $percentage;
		$json['message'] = '<p>' . sprintf( __( 'Converting... %s%%', 'tpd' ), $percentage ) . '</p>';

		return $json;
	}

}
/**
 * Detect shortcodes in the global $post.
 */
function wpdocs_detect_shortcode( $post_content, $search ) {
    global $post;
    $pattern = get_shortcode_regex();
	$gallery_src = array();
    if (   preg_match_all( '/'. $pattern .'/s', $post_content, $matches )
        && array_key_exists( 2, $matches )
        && in_array( $search, $matches[2] )
    ) {
		// shortcode is being used
		
		foreach ($matches[0] as $key => $match) {
			preg_match('/ids=\"(.*?)\"/',$match,$ids);
			if (isset($ids[1])) {
				$ids = explode(',',$ids[1]);
				foreach ($ids as $key => $id) {
					$gallery_src[$id]['url'] = wp_get_attachment_url( $id );
					$gallery_src[$id]['alt'] = get_post_meta($id, '_wp_attachment_image_alt', TRUE);
					$gallery_src[$id]['caption'] = wp_get_attachment_caption( $id );
				}
			}
		}

		return $gallery_src;
    }else{
		return false;
	}
}
/**
 * Sort the number of posts by type and create labeled array.
 *
 * @return array
 */
function tpd_count_indexed() {
	$post_types = unserialize( TPD_BLOCK_CONVERTER_TYPES );

	$indexed = array();
	foreach ( $post_types as $type ) {
		$post_type_obj     = get_post_type_object( $type );
		$label             = $post_type_obj->labels->name;
		$indexed[ $label ] = wp_count_posts( $type );
	}

	return $indexed;
}
function tpd_block_converter_count_posts( $status = '' )
{
	global $wpdb;

	$term = get_term_by( 'slug', 'classic', 'tpd_blocks_converter');
	$status = ( $status == 'any' || $status == 'all' ) ? '' : $status;
	$query = "SELECT post_status, COUNT( * ) AS num_posts FROM {$wpdb->posts}\n";
	$query .= "LEFT JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)\n";
	$query .= " WHERE ( {$wpdb->term_relationships}.term_taxonomy_id IN ($term->term_id) ) AND {$wpdb->posts}.post_type = %s";
	$query .= ($status !== '' ) ? " AND {$wpdb->posts}.post_status = '$status'\n" : "\n";
	$query .= " GROUP BY {$wpdb->posts}.post_status";
	
	$results = (array) $wpdb->get_results( $wpdb->prepare( $query, 'post' ), ARRAY_A );
    $counts  = array_fill_keys( get_post_stati(), 0 );
 
    foreach ( $results as $row ) {
        $counts[ $row['post_status'] ] = $row['num_posts'];
    }
 
	return $counts;
}
/**
 * Count indexed posts by type.
 *
 * @param string/array $type post type/types
 *
 * @return int
 */
function tpd_get_count( $type ) {
	$args = array(
		'posts_per_page' => -1,
		'post_type'      => $type,
		'meta_key'       => 'tpd_is_classic_editor',
		'meta_value'     => 1,
	);

	$posts_query = new WP_Query( $args );
	return $posts_query->post_count;
}
function tpd_save_bulk( WP_REST_Request $request )
{
	$json  = array();
	
	if ( ! empty( $request['total'] ) ) {
		$json = array(
			'error'  => false,
			'offset' => intval( $request['offset'] ),
			'total'  => intval( $request['total'] ),
		);
		foreach ( $request['postsData'] as $post ) {
			$post_data = array(
				'ID'           => $post['id'],
				'post_content' => $post['content'],
			);
			if ( ! wp_update_post( $post_data ) ) {
				$json['error'] = true;
				return $json;
			}
		}
		return $json;
	}
}

function tpd_convert_single( $data )
{

	if ( ! empty( $data['id'] ) ) {
		$post_id = intval( $data['id'] );
		$post    = get_post( $post_id );
		if ( ! $post ) {
			$json['error'] = true;
			return json_encode( $json );
		} else {
			$json['id'] = $data['id'];
			$json['content'] = wpautop( $post->post_content );
			if(wpdocs_detect_shortcode( $post->post_content, 'gallery' ) !== false){
				$json['gallery'] = wpdocs_detect_shortcode( $post->post_content, 'gallery' );
			}
            return $json;
		}
	}
}

function tpd_save_single( WP_REST_Request $request )
{
    $post_id = $request['post_id'];
    $post_content = $request['post_content'];

    if(!$post_id){
        $json['error'] = 'Post ID is missing, invalid Post.';
        return $json;
    }

    $post_id   = intval( $post_id );
    $post_data = array(
        'ID'           => $post_id,
        'post_content' => $post_content,
    );

    $json['message'] = $post_id;

    if ( ! wp_update_post( $post_data ) ) {
        $json['error'] = true;
        return $json;
    } else {
        $json['message'] = $post_id;
        return $json;
    }    

}

/**
 * Automatically index posts on creation or updating.
 */

add_action( 'post_updated', 'tpd_index_after_save', 10, 2 );
function tpd_index_after_save( $post_ID, $post_after ) {
	if ( tpd_is_classic_editor( $post_after->post_content ) ) {
		//error_log(print_r('IS classic',true));
		wp_set_object_terms( $post_ID, 'classic', 'tpd_blocks_converter');
		// update_post_meta( $post_ID, 'tpd_is_classic_editor', 1 );
	} else {
		//error_log(print_r('IS Block',true));
		wp_remove_object_terms( $post_ID, 'classic', 'tpd_blocks_converter');
		//delete_post_meta( $post_ID, 'tpd_is_classic_editor' );
	}
}